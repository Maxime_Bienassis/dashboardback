const mongoose = require('mongoose');


const Album = new mongoose.Schema(
  {
    title: String,
    release: Number,
    kind: String,
    cover_url: String,
    artiste: [String]


  });


module.exports = mongoose.model('Album', Album);
