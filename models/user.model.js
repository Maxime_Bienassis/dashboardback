const mongoose = require('mongoose');

const user = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    release: String,

  },
  {
    timestamps: true
  }
);


module.exports = mongoose.model('User', user);
