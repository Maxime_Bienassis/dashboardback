var express = require('express');
var router = express.Router();
// we import our user controller
var track = require('../controllers/track.controller');

/* GET All track */
router.get('/', track.findAll);

/* GET one user */
router.get('/:_id', track.findOne);
/* update  one user */
router.post('/:_id', track.update);

/* create  one user */
router.put('/', track.create);

/* DELETE  one user */
router.delete('/:_id', track.delete);

module.exports = router;