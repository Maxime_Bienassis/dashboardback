var express = require('express');
var router = express.Router();
// we import our user controller
var artist = require('../controllers/artist.controller');

/* GET All artist */
router.get('/', artist.findAll);

/* GET one user */
router.get('/:_id', artist.findOne);
/* update  one user */
router.post('/:_id', artist.update);

/* create  one user */
router.put('/', artist.create);

/* DELETE  one user */
router.delete('/:_id', artist.delete);

module.exports = router;