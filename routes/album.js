var express = require('express');
var router = express.Router();
// we import our user controller
var album = require('../controllers/album.controller');

/* GET All user */
router.get('/', album.findAll);

/* GET one user */
router.get('/:_id', album.findOne);
/* update  one user */
router.post('/:_id', album.update);

/* create  one user */
router.put('/', album.create);

/* DELETE  one user */
router.delete('/:_id', album.delete);

module.exports = router;