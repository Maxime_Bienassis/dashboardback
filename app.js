const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const config = require('./config/database.config');
var cors = require('cors');



// on se connecte à la base de données
mongoose.connect("mongodb://localhost:27017/Music", { useNewUrlParser: true });
const app = express();

var db = mongoose.connection;
db.once('open',function(){
    console.log("connexion à la base ok");
});


const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const userRouter = require('./routes/user');
const albumRouter = require('./routes/album');
const artistRouter = require('./routes/artist');
const trackRouter = require('./routes/track');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())

 



app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/users', usersRouter);
app.use('/albums', albumRouter);
app.use('/artists', artistRouter);
app.use('/tracks',trackRouter);


module.exports = app;
