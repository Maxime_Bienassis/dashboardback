const Artist = require('../models/artist.model.js');



// Retrieve and return all artists from the database.
exports.findAll = (req, res) => {
    Artist.find()
      .then(artists => {
        res.send(artists);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || 'Some error occurred while retrieving artists.'
        });
      });

  };
  // Update a artist identified by the artistId in the request
exports.update = (req, res) => {
  // Validate Request   
  if (!req.body.title) {
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Find artist and update it wit theh request body
  Artist.findByIdAndUpdate(
    req.params._id,
    {
      _id:req.body._id,
      title: req.body.title,
      release: req.body.release,
      kind: req.body.kind,
      cover_url: req.body.cover_url|| ''
    },
    { new: true }
  )
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artist not found with id ' + req.params._id
        });
      }
      res.send(artist);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Title not found with id ' + req.params.title
        });
      }
      return res.status(500).send({
        message: 'Error updating title with id ' + req.params.title
      });
    });
};


// Find a single artist with a artistId
exports.findOne = (req, res) => {
    Artist.findById(req.params._id)
      .then(artist => {
        if (!artist) {
          return res.status(404).send({
            message: 'title not found with id ' + req.params._id
          });
        }
        res.send(artist);
      })
      .catch(err => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: 'Title not found with id ' + req.params._id
          });
        }
        return res.status(500).send({
          message: 'Error retrieving artist with id ' + req.params._id
        });
      });
  };

  // Create and Save a new artist
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    // If firstName is not present in body reject the request by
    // sending the appropriate http code
    return res.status(400).send({
      message: 'name can not be empty'
    });
  }

  // Create a new artist
  const artist = new Artist({
    name: req.body.name,
    birth: req.body.birth,
    followers: req.body.followers || ''
  });

  // Save artist in the database
  artist
    .save()
    .then(data => {
      // we wait for insertion to be complete and we send the newly artist integrated
      res.send(data);
    })
    .catch(err => {
      // In case of error during insertion of a new artist in database we send an
      // appropriate message
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Artist.'
      });
    });
};

  exports.delete = (req, res) => {
    Artist.findByIdAndRemove(req.params._id)
      .then(artists => {
        if (!artists) {
          return res.status(404).send({
            message: 'artist not found with id ' + req.params._id
          });
        }
        res.send({ message: 'artist deleted successfully!' });
      })
      .catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: 'Artist not found with id ' + req.params._id
          });
        }
        return res.status(500).send({
          message: 'Could not delete artist with id ' + req.params._id
        });
      });
  };