const Album = require('../models/album.model.js');



// Retrieve and return all albums from the database.
exports.findAll = (req, res) => {
    Album.find()
      .then(albums => {
        res.send(albums);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || 'Some error occurred while retrieving albums.'
        });
      });

  };
  // Update a album identified by the albumId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.title) {
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Find album and update it with the request body
  Album.findByIdAndUpdate(
    req.params._id,
    {
      _id:req.body._id,
      title: req.body.title,
      release: req.body.release,
      kind: req.body.kind,
      cover_url: req.body.cover_url || ''
    },
    { new: true }
  )
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Album not found with id ' + req.params._id
        });
      }
      res.send(album);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Title not found with id ' + req.params.title
        });
      }
      return res.status(500).send({
        message: 'Error updating title with id ' + req.params.title
      });
    });
};


// Find a single album with a albumId
exports.findOne = (req, res) => {
    Album.findById(req.params._id)
      .then(album => {
        if (!album) {
          return res.status(404).send({
            message: 'title not found with id ' + req.params._id
          });
        }
        res.send(album);
      })
      .catch(err => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: 'Title not found with id ' + req.params._id
          });
        }
        return res.status(500).send({
          message: 'Error retrieving album with id ' + req.params._id
        });
      });
  };

  // Create and Save a new album
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    // If firstName is not present in body reject the request by
    // sending the appropriate http code
    return res.status(400).send({
      message: 'title can not be empty'
    });
  }

  // Create a new album
  const album = new Album({
    title: req.body.title,
    release: req.body.release,
    kind: req.body.kind,
    cover_url: req.body.cover_url ,
    artiste:req.body.artiste || ''
  });

  // Save album in the database
  album
    .save()
    .then(data => {
      // we wait for insertion to be complete and we send the newly album integrated
      res.send(data);
    })
    .catch(err => {
      // In case of error during insertion of a new album in database we send an
      // appropriate message
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Album.'
      });
    });
};

  exports.delete = (req, res) => {
    Album.findByIdAndRemove(req.params._id)
      .then(albums => {
        if (!albums) {
          return res.status(404).send({
            message: 'album not found with id ' + req.params._id
          });
        }
        res.send({ message: 'album deleted successfully!' });
      })
      .catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: 'Album not found with id ' + req.params._id
          });
        }
        return res.status(500).send({
          message: 'Could not delete album with id ' + req.params._id
        });
      });
  };