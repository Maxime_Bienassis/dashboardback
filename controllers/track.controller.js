const Track = require('../models/track.model.js');



// Retrieve and return all tracks from the database.
exports.findAll = (req, res) => {
    Track.find()
      .then(tracks => {
        res.send(tracks);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || 'Some error occurred while retrieving tracks.'
        });
      });

  };
  // Update a track identified by the trackId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.title) {
    return res.status(400).send({
      message: 'first name can not be empty'
    });
  }

  // Find track and update it with the request body
  Track.findByIdAndUpdate(
    req.params._id,
    {
      _id:req.body._id,
      title: req.body.title,
      duration: req.body.duration,
      listenings: req.body.kind,
      likes: req.body.cover_url || ''
    },
    { new: true }
  )
    .then(track => {
      if (!track) {
        return res.status(404).send({
          message: 'Track not found with id ' + req.params._id
        });
      }
      res.send(track);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Title not found with id ' + req.params.title
        });
      }
      return res.status(500).send({
        message: 'Error updating title with id ' + req.params.title
      });
    });
};


// Find a single track with a trackId
exports.findOne = (req, res) => {
    Track.findById(req.params._id)
      .then(track => {
        if (!track) {
          return res.status(404).send({
            message: 'title not found with id ' + req.params._id
          });
        }
        res.send(track);
      })
      .catch(err => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: 'Title not found with id ' + req.params._id
          });
        }
        return res.status(500).send({
          message: 'Error retrieving track with id ' + req.params._id
        });
      });
  };

  // Create and Save a new track
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    // If firstName is not present in body reject the request by
    // sending the appropriate http code
    return res.status(400).send({
      message: 'title can not be empty'
    });
  }

  // Create a new track
  const track = new Track({
    title: req.body.title,
    duration: req.body.duration,
    listenings: req.body.listenings,
    likes: req.body.likes || ''
  });

  // Save track in the database
  track
    .save()
    .then(data => {
      // we wait for insertion to be complete and we send the newly track integrated
      res.send(data);
    })
    .catch(err => {
      // In case of error during insertion of a new track in database we send an
      // appropriate message
      res.status(500).send({
        message: err.message || 'Some error occurred while creating the Track.'
      });
    });
};

  exports.delete = (req, res) => {
    Track.findByIdAndRemove(req.params._id)
      .then(tracks => {
        if (!tracks) {
          return res.status(404).send({
            message: 'track not found with id ' + req.params._id
          });
        }
        res.send({ message: 'track deleted successfully!' });
      })
      .catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: 'Track not found with id ' + req.params._id
          });
        }
        return res.status(500).send({
          message: 'Could not delete track with id ' + req.params._id
        });
      });
  };